from django.conf.urls import url
from views import api
from views import processing as prc
from views import views as vw
from views import active_ranking

app_name = 'processer'

# Views

urlpatterns = [
    url(r'^$', vw.IndexView.as_view(), name="index"),
    url(r'^article/(?P<pk>[0-9]+)/$', vw.DetailView.as_view(), name="detail"),
    url(r'^article/new/$', vw.ArticleCreate.as_view(), name="article-add" ),
    url(r'^article/delete/(?P<pk>[0-9]+)/$', vw.ArticleDelete.as_view(), name="article-delete" )]

# Processing

urlpatterns += [
    url(r'^article/process/(?P<article_id>[0-9]+)/$', prc.process, name="process"),
    url(r'^article/score/(?P<article_id>[0-9]+)/$', prc.score_associations, name="scoring"),
    url(r'^article/export/(?P<article_id>[0-9]+)/$', prc.export, name="export"),
]

# APIs

urlpatterns += [
    url(r'^articles/$', api.get_articles, name="api_articles"),
    url(r'^articles/(?P<id>[0-9]+)/$', api.get_article, name="api_article"),
    url(r'^articles/(?P<id>[0-9]+)/associations/serendipity/(?P<length>[\w\-]+)/relevance/(?P<rel>[0-9]+)/rarity/(?P<rar>[0-9]+)/top/(?P<top>[0-9]+)$', api.get_associations, name="api_associations"),
    url(r'^articles/(?P<id>[0-9]+)/length/all/associations', api.get_all_associations, name="api_associations_all")
    ]

# ActiveLearning

urlpatterns += [
    url(r'^ranking/run/$', active_ranking.run, name="ranking"),

]