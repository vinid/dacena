
from django.views.decorators.csrf import csrf_exempt
from processer.active_ranking.svmlight import SvmLight
from processer.active_ranking.active_svm import RankSVMActiveLearner
import json
import pandas as pd
from django.http import JsonResponse
"""
curl --header "Content-Type: application/json" \
  --request POST \
  --data '{
	"training_ids": [1, 2, 3, 4, 5, 6],
	"training_score": [3, 3, 1, 1, 3, 1],
	"training_features": {
		"feature_1": [5, 5, 0, 0, 5, 0],
		"feature_2": [10, 10, 0, 0, 10, 0]
	},
	"prediction_ids": [1, 2, 3, 4, 5, 6],
	"prediction_score": [3, 3, 1, 1, 3, 1],
	"prediction_features": {
		"feature_1": [5, 5, 0, 0, 5, 0],
		"feature_2": [10, 10, 0, 0, 10, 0]
	}
}' \
  http://127.0.0.1:8000/processer/ranking/run/

"""


@csrf_exempt
def run(request):

    model = SvmLight()

    rs = RankSVMActiveLearner(model)

    data = json.loads(request.body)


    ids = data["training_ids"]
    score = data["training_score"]

    training = pd.DataFrame({
        "association_id" : ids,
        "score" : score
    })

    for index, a in enumerate(data["training_features"].keys()):
        training["feature_" + str(index)] = pd.Series(data["training_features"][a])


    ids = data["prediction_ids"]

    predicting = pd.DataFrame({
        "association_id" : ids,
    })

    for index, a in enumerate(data["prediction_features"].keys()):
        predicting["feature_" + str(index)] = pd.Series(data["training_features"][a])

    training_y = training["score"]
    training = training.set_index('association_id')
    training = training.drop('score', 1)


    to_rank, best_scored, _ = rs.get_rank_and_active(training, training_y, predicting)


    return JsonResponse({"to_rank" : list(to_rank), "best_scored" : list(best_scored)}, safe=False)