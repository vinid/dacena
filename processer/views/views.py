# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.views import generic
from processer.utils.sparql_queries import  get_associations
from django.core.urlresolvers import reverse
from processer.utils import edit_utilities as ut

from processer.models import Article, Association
from processer.forms import ArticleForm

from urlparse import urlparse

class IndexView(generic.ListView):
    template_name = 'processer/index.html'
    def get_queryset(self):
        return Article.objects.all()

class DetailView(generic.DetailView):
    model = Article
    template_name = 'processer/detail.html'
    def get_context_data(self, **kwargs):
        context = super(DetailView, self).get_context_data(**kwargs)
        str_ass = []
        for a in self.object.association_set.all():
            new_str = ut.add_link_html(a.source, ut.get_name_from_url(a.source)) + " " + a.firstProperty[0] + " " + ut.get_name_from_url(a.firstProperty[1:]) + " " + a.firstProperty[0]
            if a.middleOne != None:
                new_str += " " + ut.add_link_html(a.middleOne, ut.get_name_from_url(a.middleOne)) + " " + a.secondProperty[0] + " " + ut.get_name_from_url(a.secondProperty[1:]) + " " + a.secondProperty[0]
            new_str += " " + ut.add_link_html(a.destination, ut.get_name_from_url(a.destination))
            str_ass += [new_str]
        context['str_ass'] = str_ass
        return context
    
class ArticleCreate(generic.edit.CreateView):
    model = Article
    form = ArticleForm()
    template_name = 'processer/new.html'
    fields = ['title', 'text', 'author', 'url', 'category']
    def get_success_url(self):
        return reverse('processer:process', args=[self.object.id])
    def form_valid(self,form):
        try:
            form.instance.source = urlparse(form.instance.url).hostname
        except:
            form.instance.source = form.instance.url
        return super(ArticleCreate, self).form_valid(form)
        
class ArticleDelete(generic.edit.DeleteView):
    model = Article
    def get_success_url(self):
        return reverse('processer:index')







