from __future__ import unicode_literals

from django.http import JsonResponse
from processer.models import Article, Association

def get_name(url):
    return url.split("/")[-1]

def get_articles(request):
    """
    get data from articles
    :param request:
    :return:
    """
    articles = Article.objects.all()
    build_response = []
    for a in articles:
        article = {}
        article["id"] = a.id
        article["title"] = a.title
        article["date"] = a.date
        article["author"] = a.author
        article["url"] = a.url
        article["category"] = a.category
        build_response.append(article)

    return JsonResponse(build_response, safe=False)

def get_article(request, id):
    """
    Get data from article
    :param request:
    :param id:
    :return:
    """
    a = Article.objects.filter(id=id)[0]
    article = {}
    article["id"] = a.id
    article["title"] = a.title
    article["date"] = a.date
    article["author"] = a.author
    article["url"] = a.url
    article["text"] = a.text
    article["category"] = a.category
    return JsonResponse(article, safe=False)



def get_associations(request, id, length, rel, rar, top):
    """

    :param request:
    :param id: article id
    :param length: unused parameter, needed for compatibility with old version
    :param rel: relevance value to be uses in the computation of serendipity
    :param rar: rarity value to use in the computation of serendipity
    :param top: limit number of associations
    :return:
    """
    associations = Association.objects.filter(article__id=id)

    ones = []
    twos = []
    associations = list(associations)

    ordering_associations = []
    rar = float(rar)/100.0
    rel = float(rel)/100.0

    if top == -1:
        top = len(associations)
    top = int(top)

    for association in associations:
        ordering_associations.append((association, association.rarity*rar + association.relevance*rel))

    ordering_associations = sorted(ordering_associations, key=lambda x: x[1])
    associations = list(map(lambda x : x[0], ordering_associations))
    for a in associations[:top]:
        in_asso = {}
        subject = get_name(a.source)
        first_property = a.firstProperty
        destination = get_name(a.destination)


        if a.length == 1:
            in_asso["source"] = subject
            in_asso["steps"] = {"property" : first_property,
                                "destination" : destination}
            ones.append(in_asso)
        else:
            middle = get_name(a.middleOne)
            second_property = a.secondProperty
            in_asso["source"] = subject
            in_asso["steps"] = [{"property" : first_property,
                                "destination" : middle},
                                {"property": second_property,
                                 "destination": destination}
                                ]
            twos.append(in_asso)

    return JsonResponse({"associations" : {"one" : ones, "two" : twos}}, safe=False)

def get_all_associations(request, id):
    return get_associations(None, id, "all", 50, 50, -1)
