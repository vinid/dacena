from __future__ import unicode_literals
from django.http import HttpResponseRedirect
from processer.utils.sparql_queries import  get_associations
from django.core.urlresolvers import reverse
import numpy as np
from processer.utils import edit_utilities as ut
from processer.measures import measure
from processer.models import Article, Association
from dandelion import DataTXT
import csv
from django.http import HttpResponse
from itertools import groupby as g

TODAY_TOKEN = 'f2246cc5666d4ad4a65c0032b3dd6b57'


def most_common(L):
    """
    simple function that returns the most frequent element in a list
    :param L:
    :return:
    """
    return max(g(sorted(L)), key=lambda(x, v):(len(list(v)),-L.index(x)))[0]


def compute_min_max(values):
    return np.min(values), np.max(values)

def score_associations(request, article_id):
    """
    Function used to score the semantic associations
    :param request:
    :param article_id:
    :return:
    """
    article = Article.objects.get(pk=article_id)
    associations = article.association_set.all()

    asso_dict = dict(map(lambda x: (x.id, x), associations))

    rel_eval = measure.Relevance(associations, article.text).compute_score()
    rar_eval = measure.Rarity(associations).compute_score()
    local_page_rank = measure.PageRank(associations).compute_score()
    hubs, auth = measure.HITS(associations).compute_score()
    path_informativeness = measure.PathInformativeness(associations).compute_score()
    path_pattern_info = measure.PathPatternInformativeness(associations).compute_score()
    temporal_relevance = measure.PageViewsArticleDay(associations, article.date).compute_score()
    global_page_rank = measure.DBpediaPageRank(associations).compute_score()

    relevance_scores = rel_eval.values()
    rarity_scores = rar_eval.values()
    local_page_rank_scores = local_page_rank.values()
    hubs_scores = hubs.values()
    auth_scores = auth.values()
    path_info_scores = path_informativeness.values()
    path_pattern_info_scores = path_pattern_info.values()
    temporal_relevance_scores = temporal_relevance.values()
    global_page_rank_scores = global_page_rank.values()

    rel_min, rel_max = compute_min_max(relevance_scores)
    rar_min, rar_max = compute_min_max(rarity_scores)
    lc_pr_min, lc_pr_max = compute_min_max(local_page_rank_scores)
    hubs_min, hubs_max = compute_min_max(hubs_scores)
    auth_min, auth_max = compute_min_max(auth_scores)
    path_info_min, path_info_max = compute_min_max(path_info_scores)
    path_pattern_info_min, path_pattern_info_max = compute_min_max(path_pattern_info_scores)
    temporal_relevance_min, temporal_relevance_max = compute_min_max(temporal_relevance_scores)
    global_page_rank_min, global_page_rank_max = compute_min_max(global_page_rank_scores)

    # Normalization in [0,1] and saving

    for id in rel_eval:
        asso_dict[id].relevance = (rel_eval[id] - rel_min) / (rel_max - rel_min)
        asso_dict[id].rarity = 1 - ((rar_eval[id] - rar_min) / (rar_max - rar_min))
        asso_dict[id].local_page_rank = ((local_page_rank[id] - lc_pr_min) / (lc_pr_max - lc_pr_min))
        asso_dict[id].hub = ((hubs[id] - hubs_min) / (hubs_max - hubs_min))
        asso_dict[id].auth = ((auth[id] - auth_min) / (auth_max - auth_min))
        asso_dict[id].path_info = ((path_informativeness[id] - path_info_min) / (path_info_max - path_info_min))
        asso_dict[id].path_pattern_info = ((path_pattern_info[id] - path_pattern_info_min) / (path_pattern_info_max - path_pattern_info_min))
        asso_dict[id].temporal_relevance = ((temporal_relevance[id] - temporal_relevance_min) / (temporal_relevance_max - temporal_relevance_min))
        asso_dict[id].global_page_rank = ((global_page_rank[id] - global_page_rank_min) / (global_page_rank_max - global_page_rank_min))
        asso_dict[id].save()

    return HttpResponseRedirect(reverse('processer:detail', args=(article_id,)))


def export(request, article_id):
    """
    Export associations in CSV format
    :param request:
    :param article_id:
    :return:
    """
    # Create the HttpResponse object with the appropriate CSV header.
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="associations.csv"'
    associations = Association.objects.filter(article__id=article_id)
    writer = csv.writer(response)

    for a in associations:
        new_str = ut.get_name_from_url(a.source) + "," + ut.get_name_from_url(
            a.firstProperty[0]) + "," + ut.get_name_from_url(
            a.firstProperty[1:]) + "," + a.firstProperty[0]
        if a.middleOne != None: new_str += "," + ut.get_name_from_url(a.middleOne) + "," + a.secondProperty[
            0] + "," + ut.get_name_from_url(a.secondProperty[1:]) + "," + a.secondProperty[0]
        new_str += "," + ut.get_name_from_url(a.destination)
        writer.writerow([new_str])

    return response


def process(request, article_id):
    """
    Process an article
    :param request:
    :param article_id:
    :return:
    """
    datatxt = DataTXT(app_id=TODAY_TOKEN, app_key=TODAY_TOKEN)
    article = Article.objects.get(pk=article_id)
    response = datatxt.nex(article.text, lang='en', include='lod')
    entity_set = set()
    entity_list = []

    for annotation in response.annotations:
        if annotation.lod.dbpedia not in entity_set:
            article.entity_set.create(title=annotation.title, uri=annotation.lod.dbpedia)
            entity_list.append(annotation.lod.dbpedia)
            entity_set.add(annotation.lod.dbpedia)

    source = most_common(entity_list) # get the most common entity as main subject

    for destination in entity_set:
        if destination == source:
            continue
        associations = get_associations(source, destination)
        if len(associations) != 0:
            for a in associations:
                article.association_set.create(length=a[0], source=source, destination=destination, middleOne=a[1],
                                               firstProperty=a[2], secondProperty=a[3])

    return HttpResponseRedirect(reverse('processer:detail', args=(article_id,)))

