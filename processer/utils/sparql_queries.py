from SPARQLWrapper import SPARQLWrapper, JSON
QUERY_LIMIT = "1000"

def get_filters(string):
    return """ FILTER(STRSTARTS(str({0}), "http://dbpedia.org/ontology/"))
        FILTER(!REGEX(str({0}),"wiki"))
        FILTER(!STRSTARTS(str({0}), "http://dbpedia.org/ontology/abstract"))
        FILTER(!STRSTARTS(str({0}), "http://dbpedia.org/ontology/thumbnail"))
        FILTER(!STRSTARTS(str({0}), "http://dbpedia.org/ontology/logo")) . """.format(string)


def get_filters_literal(string):
    return """ FILTER(STRSTARTS(str({0}), "http://dbpedia.org/resource/")) """.format(string)

def get_associations(source, destination):
    associations = []
    sparql = SPARQLWrapper('http://dbpedia.org/sparql')

    # Query 1: s -> d

    sparql.setQuery("""
        select ?predicate
        where { <""" + source + "> ?predicate <" + destination + "> ." +
                    get_filters("?predicate") + "} limit " + QUERY_LIMIT)
    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()

    for result in results["results"]["bindings"]:
        associations += [[1, None, "R:" + result["predicate"]["value"], None]]

    # Query 2: d -> s

    sparql.setQuery("""
        select ?predicate
        where { <""" + destination + "> ?predicate <" + source + "> ." +
                    get_filters("?predicate") + "} limit " + QUERY_LIMIT)
    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()

    for result in results["results"]["bindings"]:
        associations += [[1, None, "L:" + result["predicate"]["value"], None]]

    # Query 3: s -> ?x -> d

    sparql.setQuery("""
        select ?predicate1 ?object ?predicate2
        where { <""" + source + "> ?predicate1 ?object ." +
                    "?object ?predicate2 <" + destination + "> ." +
                    get_filters("?predicate1") + get_filters("?predicate2") + " " + get_filters_literal(
        "?object") + "} limit " + QUERY_LIMIT)
    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()

    for result in results["results"]["bindings"]:
        associations += [
            [2, result["object"]["value"], "R:" + result["predicate1"]["value"], "R:" + result["predicate2"]["value"]]]

    # Query 4: s <- ?x <- d

    sparql.setQuery("""
        select ?predicate1 ?object ?predicate2
        where {  ?object ?predicate1 <""" + source + "> ." +
                    "<" + destination + "> ?predicate2 ?object ." +
                    get_filters("?predicate1") + get_filters("?predicate2") + " " + get_filters_literal(
        "?object") + "} limit " + QUERY_LIMIT)
    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()

    for result in results["results"]["bindings"]:
        associations += [
            [2, result["object"]["value"], "L:" + result["predicate1"]["value"], "L:" + result["predicate2"]["value"]]]

    # Query 5: s <- ?x -> d

    sparql.setQuery("""
        select ?predicate1 ?object ?predicate2
        where { ?object ?predicate1 <""" + source + "> ." +
                    "?object ?predicate2 <" + destination + "> ." +
                    get_filters("?predicate1") + get_filters("?predicate2") + " " + get_filters_literal(
        "?object") + "} limit " + QUERY_LIMIT)
    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()

    for result in results["results"]["bindings"]:
        associations += [
            [2, result["object"]["value"], "L:" + result["predicate1"]["value"], "R:" + result["predicate2"]["value"]]]

    # Query 6: s -> ?x <- d

    sparql.setQuery("""
        select ?predicate1 ?object ?predicate2
        where { <""" + source + "> ?predicate1 ?object ." +
                    "<" + destination + "> ?predicate2 ?object ." +
                    get_filters("?predicate1") + get_filters("?predicate2") + " " + get_filters_literal(
        "?object") + "} limit " + QUERY_LIMIT)
    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()

    for result in results["results"]["bindings"]:
        associations += [
            [2, result["object"]["value"], "R:" + result["predicate1"]["value"], "L:" + result["predicate2"]["value"]]]

    return associations

def get_abstract(source):
    try:
        sparql = SPARQLWrapper("http://dbpedia.org/sparql")
        q = """
                SELECT ?abstract
                WHERE { <""" + source.replace(" ", "+") + """> <http://dbpedia.org/ontology/abstract> ?abstract
                FILTER langMatches( lang(?abstract), "en" )}
            """
        sparql.setQuery(q)
        sparql.setReturnFormat(JSON)
        results = sparql.query().convert()

    except Exception:
        return " "
    import os
    dir_path = os.path.dirname(os.path.realpath(__file__)) + "/../measures/"
    for result in results["results"]["bindings"]:
        try:
            abstract = result["abstract"]["value"]
            with open(dir_path + 'stopwords.txt', 'r') as f:
                lines = f.read().splitlines()
            text = abstract
            text = ' '.join([word for word in text.split() if word not in lines])
            return text
        except Exception as e:
            print(e)
            abstract = " "
            return abstract
    return " "  # if nothing was found


