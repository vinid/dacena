
import networkx as nx

def build_graph_from_associations(associations):
    graph = nx.MultiDiGraph()
    for i, association in enumerate(associations):
        if association.length == 2:
            if association.firstProperty[0] == "R":
                graph.add_edge(association.source, association.middleOne)
            if association.firstProperty[0] == "L":
                graph.add_edge(association.middleOne, association.source)

            if association.secondProperty[0] == "R":
                graph.add_edge(association.middleOne, association.destination)
            if association.secondProperty[0] == "L":
                graph.add_edge(association.destination, association.middleOne)
        else:
            if association.firstProperty[0] == "R":
                graph.add_edge(association.source, association.destination)
            if association.firstProperty[0] == "L":
                graph.add_edge(association.destination, association.source)
    return graph


def build_graph_with_properties(associations):
    graph = nx.MultiDiGraph()
    for i, association in enumerate(associations):
        if association.length == 2:
            if association.firstProperty[0] == "R":
                graph.add_edges_from([(association.source, association.middleOne, {'property': association.firstProperty[2:]})])
            if association.firstProperty[0] == "L":
                graph.add_edges_from([(association.middleOne, association.source, {'property': association.firstProperty[2:]})])

            if association.secondProperty[0] == "R":
                graph.add_edges_from([(association.middleOne, association.destination, {'property': association.secondProperty[2:]})])
            if association.secondProperty[0] == "L":
                graph.add_edges_from([(association.destination, association.middleOne, {'property': association.secondProperty[2:]})])
        else:
            if association.firstProperty[0] == "R":
                graph.add_edges_from([(association.source, association.destination, {'property': association.firstProperty[2:]})])
            if association.firstProperty[0] == "L":
                graph.add_edges_from([(association.destination, association.source, {'property': association.firstProperty[2:]})])
    return graph
