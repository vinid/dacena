def get_name_from_url(url):
    """
    something.org/url -> url
    :param url:
    :return:
    """
    return url.split("/")[-1]

def add_link_html(url, name):
    """
    add link for html display
    :param url:
    :param name:
    :return:
    """
    return "<a href=\"" + url + "\">" + name + "</a>"

def remove_dbpedia_uri(uri):
    return uri.replace("http://dbpedia.org/resource/", "").replace("http://dbpedia.org/ontology/", "").replace("http://dbpedia.org/property/", "")