from django.core.management.base import BaseCommand, CommandError
import re
from processer.models import Abstract



class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument("-a", '--abstract_path', type=str)
        parser.add_argument("-c", '--csv_output', type=str)

    def handle(self, *args, **options):
        path_abstract = options['abstract_path']
        output_path = options['csv_output']

        with open(output_path, "w") as csv_file:
            csv_file.write("uri,text" + "\n")

            with open(path_abstract, "r") as abstract_file:
                i = 0
                for line in abstract_file:
                    if i == 0:
                        i = i + 1
                        continue
                    if i%10000 == 0:
                        print("Loaded " + str(i) + " abstracts")
                    i = i +1
                    abstract = re.findall('"([^"]*)"', line)[0]
                    uri = line.split(" ")[0].replace("<http://dbpedia.org/resource/", "").replace(">", "")

                    ab = Abstract()
                    ab.uri = uri
                    ab.text = abstract
                    ab.save()
