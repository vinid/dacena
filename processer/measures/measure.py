"""
Code that contains the evaluation measures
"""

from processer.utils import sparql_queries
from processer.utils import edit_utilities as edut
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.feature_extraction.text import TfidfVectorizer
from processer.models import Abstract
import os
import csv
import logging
from processer.utils import utils as uu
import numpy as np
from mwviews.api import PageviewsClient
from SPARQLWrapper import SPARQLWrapper, JSON

import scipy as sci
from datetime import datetime
from networkx.algorithms import *
from math import log10
from collections import Counter
from math import log

class Measure(object):
    """
    Partent class of all the measures defined
    """
    def __init__(self, associations):
        logging.info("Running " + type(self).__name__)
        self.associations = associations
        pass

    def compute_score(self):
        pass


class Relevance(Measure):
    """
    Computes the relevance of each association with respect to a given corpus.
    """
    def __init__(self, associations, article_text):
        super(Relevance, self).__init__(associations)
        self.abstract_cache = {}
        self.article_text = article_text

    def compute_score(self):
        """

        :return:
        """
        dictionary = self.build_document_matrix()
        texts_list = dictionary.values()
        tfidf_vectorizer = TfidfVectorizer()

        tfidf_matrix = tfidf_vectorizer.fit_transform(texts_list)

        #compute the cosine between the first vector (the document) with respect all the others
        cosine_result = cosine_similarity(tfidf_matrix[0:1], tfidf_matrix[1:])[0]

        keys = dictionary.keys()
        keys.pop(0)  # remove article key
        final_result = dict(zip(keys, cosine_result))  # combine IDs with their score

        for key, value in final_result.iteritems():
            final_result[key] = value
        return final_result


    def build_document_matrix(self):
        """
        Generates the document dictionary that has to be passed to TFIDF vectorizer
        :return:
        """
        dictionary = {}
        dictionary[0] = self.article_text
        for i, association in enumerate(self.associations):
            string = ""
            listOfEntity = set()
            if association.length == 2:
                listOfEntity.add(association.middleOne)
            listOfEntity.add(association.source)
            listOfEntity.add(association.destination)

            for entity in listOfEntity:
                entity = self.get_abstract(entity)
                string = string + entity

            dictionary[association.id] = string.encode("utf-8")

        return dictionary


    def get_abstract(self, source):
        if source in self.abstract_cache:
            return self.abstract_cache[source]
        else:
            try:
                abstract = Abstract.objects.filter(uri=source)[0]
                self.abstract_cache[source] = abstract
            except:
                to_return = sparql_queries.get_abstract(source)
                self.abstract_cache[source] = to_return
            return to_return


class Rarity(Measure):
    """
    Implements the rarity measures.
    Reads from a file that contains the frequency of each property in DBpedia and computes the average
    frequency
    """
    def __init__(self, associations):
        super(Rarity, self).__init__(associations)

        __location__ = os.path.realpath(
            os.path.join(os.getcwd(), os.path.dirname(__file__)))
        file = csv.reader(open(os.path.join(__location__, "freq_ontology_properties.txt")))
        self.propertiesDictionary = {}
        for row in file:
           k, v = row
           self.propertiesDictionary[k] = int(v)

        for key, value in self.propertiesDictionary.iteritems():
            self.propertiesDictionary[key] = value

    def get_rarity_value(self, property):
        if property in self.propertiesDictionary:
            return self.propertiesDictionary[property]
        else:
          return 0

    def compute_score(self):
        final_result = {}
        for i, association in enumerate(self.associations):

            list_of_values = []
            list_of_values.append(self.get_rarity_value(association.firstProperty[2:]))

            if association.length == 2:
                list_of_values.append(self.get_rarity_value(association.secondProperty[2:]))

            mean_value = np.mean(list_of_values)

            final_result[association.id] = mean_value
        return final_result



class PageRank(Measure):
    def __init__(self, associations):
        super(PageRank, self).__init__(associations)

    def compute_score(self):
        final_result = {}
        graph = uu.build_graph_from_associations(self.associations)
        pr = pagerank_scipy(graph, alpha=0.9, max_iter=1000)
        for i, association in enumerate(self.associations):
            list_of_values = []

            list_of_values.append(pr[association.source])
            list_of_values.append(pr[association.destination])

            if association.length == 2:
                list_of_values.append(pr[association.middleOne])

            mean_value = np.mean(list_of_values)

            final_result[association.id] = mean_value
        return final_result


class HITS(Measure):
    def __init__(self, associations):
        super(HITS, self).__init__(associations)

    def compute_score(self):
        final_result_hub = {}
        final_result_auth = {}
        graph = uu.build_graph_from_associations(self.associations)
        pr = hits_scipy(graph, max_iter=1000)

        hubs = pr[0]
        auths = pr[1]

        # Hub and auth
        for i, association in enumerate(self.associations):
            list_of_values_hubs = []

            list_of_values_hubs.append(hubs[association.source])
            list_of_values_hubs.append(hubs[association.destination])

            if association.length == 2:
                list_of_values_hubs.append(hubs[association.middleOne])

            mean_value_hub = np.mean(list_of_values_hubs)

            final_result_hub[association.id] = mean_value_hub

            list_of_values_auth = []

            list_of_values_auth.append(auths[association.source])
            list_of_values_auth.append(auths[association.destination])

            if association.length == 2:
                list_of_values_auth.append(auths[association.middleOne])

            mean_value_auth = np.mean(list_of_values_auth)

            final_result_auth[association.id] = mean_value_auth

        return final_result_hub,    final_result_auth




class PathInformativeness(Measure):
    def __init__(self, associations):
        super(PathInformativeness, self).__init__(associations)
        logging.info("Started Path Informativeness Measure")

        self.associations = associations
        self.graph = uu.build_graph_with_properties(associations)

        self.total_triples = len(self.graph.edges())
        self.list_of_all_edges = self.all_edges()
        self.dict_of_all_edges_counted = dict(Counter(self.list_of_all_edges))

    def all_edges(self):
        edges = list(set(self.graph.edges()))
        list_of_edges = []
        for edge in edges:
            for key, value in self.graph.get_edge_data(*edge).iteritems():
                list_of_edges.append(value['property'])
        return list_of_edges

    def get_edges_properties(self, node, type):
        list_of_properties = []
        if type == "inner":
            edges = list(set(self.graph.in_edges(node))) # remove duplicates
        elif type == "outer":
            edges = list(set(self.graph.out_edges(node))) # remove duplicates
        else:
            raise NotImplementedError
        for edge in edges:
            dict_property = self.graph.get_edge_data(*edge)
            for key, value in dict_property.iteritems():
                list_of_properties.append(value['property'])
        return list_of_properties

    def path_info(self, node, property, type):
        if type == "inner":
            inner = self.get_edges_properties(node, "inner")
        elif type == "outer":
            inner = self.get_edges_properties(node, "outer")
        else:
            raise NotImplementedError
        count_of_prop = inner.count(property)

        number_of_properties_in_node = len(inner)
        #print(count_of_prop, node, property)

        pf = count_of_prop / float(number_of_properties_in_node + 0.000001)
        idf = log10(self.total_triples / self.dict_of_all_edges_counted[property])

        return pf * idf

    def compute_score(self):
        dictionary = {}
        for i, association in enumerate(self.associations):
            if i % 50 == 0:
                logging.info("\tAt number " + str(i))
            score = 0



            if association.length == 1:
                score += (self.path_info(association.source, association.firstProperty[2:], "outer") +
                          self.path_info(association.destination, association.firstProperty[2:], "inner")) / 2

            if association.length == 2:
                score += (self.path_info(association.source, association.firstProperty[2:], "outer") +
                          self.path_info(association.middleOne, association.firstProperty[2:], "inner")) / 2

                score += (self.path_info(association.middleOne, association.secondProperty[2:], "outer") +
                          self.path_info(association.destination, association.secondProperty[2:], "inner")) / 2

            score = score / association.length
            dictionary[association.id] = score
        logging.info("Ended Path Informativeness Measure")
        return dictionary


class PathPatternInformativeness(Measure):
    def __init__(self, associations):
        super(PathPatternInformativeness, self).__init__(associations)
        self.associations = associations
        self.dictionary_property = {}
        self.count_patterns = 0

    def all_patterns(self):
        patterns = []
        for association in self.associations:
            if association.length == 1:
                patterns.append([association.firstProperty])
            if association.length == 2:
                patterns.append([association.firstProperty, association.secondProperty])

        self.dictionary_property = dict(Counter(map(tuple, patterns)))
        self.count_patterns = len(self.dictionary_property)
        return patterns

    def compute_score(self):

        dictionary_result = {}
        self.all_patterns()

        for association in self.associations:
            if association.length == 1:
                value = self.dictionary_property[(association.firstProperty,)]
            if association.length == 2:
                value = self.dictionary_property[(association.firstProperty, association.secondProperty)]
            dictionary_result[association.id] =  log(self.count_patterns/value)
        return dictionary_result


class PageViewsArticleDay(Measure):
    def __init__(self, associations, day):
        super(PageViewsArticleDay, self).__init__(associations)
        self.p = PageviewsClient(user_agent="<person@organization.org> Selfie, Cat, and Dog analysis")
        print(day, type(day))
        self.day = day.strftime("%Y%m%d00")
        self.associations = associations
        self.cache = {}
        logging.info("Strated PageViews Measure")

    def callAPIGetValue(self, entity):
        try:
            _, value =  dict(self.p.article_views('en.wikipedia', articles=[entity], agent='user', access="all-access", granularity='daily',
                                        start = str(self.day), end = str(self.day))).popitem()
            #logging.info("\t" + str(value.values()[0]))
            self.cache[entity] = value.values()[0]
            return value.values()[0]
        except BaseException as e:
            print logging.error("errore " + str(e))
            return 0

    def compute_score(self):
        """
        Computes the pageviews values for each association
        :return: a dictionary in which there are the association with the respective values
        """
        dictionary = {}
        for i, association in enumerate(self.associations):
            if(i%100==0):
                logging.info("\tAt number " + str(i))
            score = []

            listOfEntity = set()  # list of entity for each association
            listOfEntity.add(edut.remove_dbpedia_uri(association.source))
            listOfEntity.add(edut.remove_dbpedia_uri(association.destination))

            if association.length == 2:
                listOfEntity.add(edut.remove_dbpedia_uri(association.middleOne))

            for entity in listOfEntity:
                if entity in self.cache:
                    cache_value = self.cache[entity]
                    logging.info("PageView value from cache:  " + entity + " " + str(cache_value))
                    value = cache_value
                else:
                    value = self.callAPIGetValue(entity)
                score.append(value)  # appending score to list

            mean_page_view = sci.mean(score)
            dictionary[association.id] = mean_page_view
        return dictionary


class DBpediaPageRank(Measure):
    """
    DBpedia Page Rank compuing using sparql endpoint on dacena.
    """
    def __init__(self, associations):
        super(DBpediaPageRank, self).__init__(associations)
        """
        Object initialization
        :param associations: associations array
        :return:
        """
        self.associations = associations
        self.cache = {}
        logging.info("Started DBpediaPageRank Measure")

    def compute_score(self):
        dictionary = {}
        for i, association in enumerate(self.associations):  # for each association
            if(i%1000==0):
                logging.info("\tAt number " + str(i))

            listOfEntity = set()  # list of entity for each association
            listOfEntity.add(edut.remove_dbpedia_uri(association.source))
            listOfEntity.add(edut.remove_dbpedia_uri(association.destination))

            if association.length == 2:
                listOfEntity.add(edut.remove_dbpedia_uri(association.middleOne))

            # here we got the list of entity for each association
            score = []
            for entity in listOfEntity:
                value = self.getPageRankValue(entity)
                if value:
                    score.append(float(value))  # appending score to list

            meanPageRank = sci.mean(score)
            # savind data
            dictionary[association.id] = meanPageRank

        return dictionary


    def getPageRankValue(self, entity):
        #entity = urllib.quote_plus(entity.encode("utf-8")) #quoting chars and encoding in utf-8
        entity = "http://dbpedia.org/resource/" + entity
        if entity in self.cache:
            cache_value = self.cache[entity]
            logging.info("PageRank value from cache:  " + entity + " " + str(cache_value))
            return cache_value
        else:
            try:
                sparql = SPARQLWrapper("http://dbpedia.org/sparql")
                sparql.addDefaultGraph("http://dbpedia.org")
                q ="""
                        PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#>
                        PREFIX dbo:<http://dbpedia.org/ontology/>
                        PREFIX vrank:<http://purl.org/voc/vrank#>
                        select ?pagerank
                        FROM <http://dbpedia.org>
                        FROM <http://people.aifb.kit.edu/ath/#DBpedia_PageRank>
                         where {

                        <""" + entity + """> vrank:hasRank ?v.
                        ?v vrank:rankValue ?pagerank
                        }
                """
                sparql.setQuery(q)
                sparql.setReturnFormat(JSON)
                results = sparql.query().convert()
                for result in results["results"]["bindings"]:
                    pagerank = result["pagerank"]["value"] #get value
                    self.cache[entity] = pagerank
                    logging.info("PageRank from server:  " + entity + " " + str(pagerank))
                    if pagerank:
                        return pagerank
                    else:
                        return 0
            except Exception as e:
                logging.error("Error for entity "+ entity + " " + str(e))
                pagerank = 0  # setting pagerank to zero
                self.cache[entity] = 0
                return pagerank