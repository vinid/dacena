# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from django.urls import reverse

CATEGORY_CHOICES = (('sport','Sport'), ('politics','Politics'), ('music','Music'), ('technology','Technology'),('miscellanues','Miscellanues'),)

class Abstract(models.Model):
    uri = models.CharField(max_length=255, primary_key=True)
    text = models.TextField()

class Article(models.Model):
    title = models.CharField(max_length=255)
    text = models.TextField()
    author = models.CharField(max_length=255)
    date = models.DateTimeField('date published', auto_now_add=True)
    source = models.CharField(max_length=255, null=True)
    url = models.CharField(max_length=255)
    category = models.CharField(max_length=255, choices=CATEGORY_CHOICES)
    is_processed = models.IntegerField(default=0)
    
    def __unicode__(self):
        return self.title
    
    def get_absolute_url(self):
        return reverse('processer:detail', kwargs={'pk': self.pk})

class Entity(models.Model):
    article = models.ForeignKey(Article, on_delete=models.CASCADE)
    title = models.CharField(max_length=255)
    uri = models.CharField(max_length=255)
    
    def __unicode__(self):
        return self.title
        
class Association(models.Model):
    length = models.IntegerField()
    article = models.ForeignKey(Article, on_delete=models.CASCADE)
    source = models.CharField(max_length=255)
    destination = models.CharField(max_length=255)
    middleOne = models.CharField(max_length=255, null=True)
    firstProperty = models.CharField(max_length=255)
    secondProperty = models.CharField(max_length=255, null=True)
    relevance = models.FloatField(null=True)
    rarity = models.FloatField(null=True)
    local_page_rank = models.FloatField(null=True)
    auth = models.FloatField(null=True)
    hub = models.FloatField(null=True)
    path_info = models.FloatField(null=True)
    path_pattern_info = models.FloatField(null=True)
    temporal_relevance = models.FloatField(null=True)
    global_page_rank = models.FloatField(null=True)