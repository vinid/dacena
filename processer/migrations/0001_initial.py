# -*- coding: utf-8 -*-
# Generated by Django 1.11.13 on 2019-03-08 10:24
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Article',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=255)),
                ('text', models.TextField()),
                ('author', models.CharField(max_length=255)),
                ('date', models.DateTimeField(auto_now_add=True, verbose_name='date published')),
                ('source', models.CharField(max_length=255, null=True)),
                ('url', models.CharField(max_length=255)),
                ('category', models.CharField(choices=[('sport', 'Sport'), ('politics', 'Politics'), ('music', 'Music'), ('technology', 'Technology'), ('miscellanues', 'Miscellanues')], max_length=255)),
                ('is_processed', models.IntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='Association',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('length', models.IntegerField()),
                ('source', models.CharField(max_length=255)),
                ('destination', models.CharField(max_length=255)),
                ('middleOne', models.CharField(max_length=255, null=True)),
                ('firstProperty', models.CharField(max_length=255)),
                ('secondProperty', models.CharField(max_length=255, null=True)),
                ('relevance', models.FloatField(null=True)),
                ('rarity', models.FloatField(null=True)),
                ('article', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='processer.Article')),
            ],
        ),
        migrations.CreateModel(
            name='Entity',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=255)),
                ('uri', models.CharField(max_length=255)),
                ('article', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='processer.Article')),
            ],
        ),
    ]
