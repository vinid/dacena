# README #

## DaCENA

DaCENA currently uses django+python2.7 and [dandelion](http://dandelion.eu/).

## Installation
`pip install -r requirements.txt `

## Run
`python manage.py runserver`

## API

## Get Articles
`http://127.0.0.1:8000/processer/articles/`

## Get Article
`http://127.0.0.1:8000/processer/articles/<article_id>/`

## Get Associations
`http://127.0.0.1:8000/processer/articles/<article_id>/associations/serendipity/all/relevance/50/rarity/50/top/100`

## Score the associations of an Article
`http://127.0.0.1:8000/processer/article/score/<article_id>`

## Active Learning to Rank

Il processo è stato staccato dal backend in modo che possa essere utilizzato anche per altri task. 
Dovrebbe essere abbastanza generico per diversi contesti. Per fare ranking e ottenere gli active sample
bisogna chiamare la rispettiva API passando nella richiesta anche un json contenente id, score e feature 
per il training e id e feature per i dati da predire.

```
curl --header "Content-Type: application/json" \
  --request POST \
  --data '{
	"training_ids": [1, 2, 3, 4, 5, 6],
	"training_score": [3, 3, 1, 1, 3, 1],
	"training_features": {
		"feature_1": [5, 5, 0, 0, 5, 0],
		"feature_2": [10, 10, 0, 0, 10, 0]
	},
	"prediction_ids": [1, 2, 3, 4, 5, 6],
	"prediction_score": [3, 3, 1, 1, 3, 1],
	"prediction_features": {
		"feature_1": [5, 5, 0, 0, 5, 0],
		"feature_2": [10, 10, 0, 0, 10, 0]
	}
}
```
In output verranno fornite le osservazioni su cui fare active learning e le osservazioni che hanno 
ranking più elevato



